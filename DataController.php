<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DataController extends Controller
{
    public $dataTableArray = [];
    public function __construct($dataArray)
    {
        $this->dataTableArray = $dataArray;
    }

    public function toTable()
    {
        $html = '<table>';
        foreach ($this->dataTableArray as $key=>$value){
            $html .= '<tr>';
            foreach ($value as $key2 => $value2){
                $html .= '<td>'. $value2 .'</td>';
            }
            $html .= '</tr>';
        }
        $html .= '</table>';

        echo $html;exit;
    }

    public function addColumn($columnName, $columnAction)
    {
        if(is_callable($columnAction)){
            $arrayOfProcessing = $this->dataTableArray;
            foreach ($arrayOfProcessing as $key => $value){
                $result = call_user_func($columnAction,$value);
                $arrayOfProcessing[$key][$columnName] = $result;
            }
            $this->dataTableArray = $arrayOfProcessing;
        }
        return $this;
    }

    public function removeColumn($columnName)
    {
        $arrayOfProcessing = $this->dataTableArray;
        foreach ($arrayOfProcessing as $key => $value){
            unset($arrayOfProcessing[$key][$columnName]);
        }
        $this->dataTableArray = $arrayOfProcessing;
        return $this;
    }

    public function editColumn($columnName,$columnAction)
    {
        if(is_callable($columnAction)){
            $arrayOfProcessing = $this->dataTableArray;
            foreach ($arrayOfProcessing as $key => $value){
                $result = call_user_func($columnAction,$value);
                $arrayOfProcessing[$key][$columnName] = $result;
            }
            $this->dataTableArray = $arrayOfProcessing;
        }
        return $this;
    }

    public function toJson()
    {
        echo  json_encode($this->dataTableArray);exit;
    }

}
