<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\DataController;

class TableController extends Controller
{

    public function callTable()
    {
        $tableDataArray = [
            [
                'id' => 1,
                'name' => 'jim',
                'gender' => 'Male',
            ],
            [
                'id' => 2,
                'name' => 'mehedi',
                'gender' => 'Male',
            ],
            [
                'id' => 3,
                'name' => 'sadiya',
                'gender' => 'FeMale',
            ]
        ];

        $tableData = new DataController($tableDataArray);

        $tableData->addColumn('action',function($data){
                    return "<button id=".$data['id'].">Action</button>";
                })
            ->editColumn('name',function($data){
                if($data['id'] == 1){
                    return "jim";
                }else{
                    return "Mehedi";
                }
            })
            ->removeColumn('id')
            ->toTable();

//        $tableData->addColumn('action',function($data){
//            return "action";
//        })
//            ->editColumn('name',function($data){
//                if($data['id'] == 1){
//                    return "jim";
//                }else{
//                    return "Mehedi";
//                }
//            })
//            ->removeColumn('id')
//            ->toJson();

    }
    
}
